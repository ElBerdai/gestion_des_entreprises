
<?php


use Carbon\Carbon;


?>
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bonjour {{ Auth::user()->name}} !</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                 <div class="grid-container">
  <div>Nombre des entreprises ajoutées récemment</div>
  <div>Nombre de projets affectés récement</div>
  <div class="num" >
                      <a  href="/companies">  {{ DB::table('companies')->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->count() }}<span>entreprises</span></a></div>
  <div class="num">   <a  href="/projects"> {{ DB::table('projects')->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->count() }}<span>projets</span></a></div>
 
 
  
</div>
                    
                   

               
                 </div>
                </div>

            </div>
        </div> 
    </div>
</div>
@endsection

<style>
.grid-container {
  display: grid;
  grid-template-columns: auto auto ;
  grid-template-rows: auto 200px;
  grid-gap: 10px;
  background-color: #e6efed;
  padding: 10px;
}
.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
.grid-container > .num{  font-size: 90px;
}
.grid-container > .num > a{   background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 90px;
  text-decoration:none;

}
.grid-container > .num >a >span{  font-size: 20px;
}
</style>