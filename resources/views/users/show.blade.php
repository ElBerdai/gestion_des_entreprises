@extends('layouts.app')

@section('content')
     
     <div class="row col-md-9 col-lg-9 col-sm-9 pull-left ">
      <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
      <!-- Jumbotron -->
      <div class="well well-lg" >
        <h1>{{ $project->name }}</h1>
        <p class="lead">{{ $project->description }}</p>
       <!-- <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p> -->
      </div>

      <!-- Example row of columns -->
      <div class="row  col-md-12 col-lg-12 col-sm-12" style="background: white; margin: 10px; ">
       <!--<a href="/projects/create/{{ $project->id }}" class="pull-right btn btn-default btn-sm" >Ajouter un projet</a>-->
      <br>
@include('partials.comments')
     <div class="row container-fluid">

<form method="post" action="{{ route('comments.store') }}">
                            {{ csrf_field() }}


                          <input type="hidden" name="commentable_type" value="App\Project">
                            <input type="hidden" name="commentable_id" value="{{$project->id}}">

                            <div class="form-group">
                                <label for="comment-content">Commentaire</label>
                                <textarea placeholder="Entrer commentaire" 
                                          style="resize: vertical" 
                                          id="comment-content"
                                          name="body"
                                          rows="3" spellcheck="false"
                                          class="form-control autosize-target text-left">

                                          
                                          </textarea>
                            </div>

                            
                            <div class="form-group">
                                <label for="comment-content">Preuve de votre travail(lien)</label>
                                <textarea placeholder="Enter url or screenshots" 
                                          style="resize: vertical" 
                                          id="comment-content"
                                          name="url"
                                          rows="2" spellcheck="false"
                                          class="form-control autosize-target text-left">

                                          
                                          </textarea>
                            </div>


                            <div class="form-group">
                                <input type="submit" class="btn btn-primary"
                                       value="Envoyer"/>
                            </div>
                        </form>
   


                        </div>

                 <!--@foreach($project->comments as $comment)
        <div class="col-lg-4 col-md-4 col-sm-4">
          <h2>{{ $comment->body }}</h2>
          <p class="text-danger"> {{$comment->url}} </p>
          <p><a class="btn btn-primary" href="/projects/{{ $project->id }}" role="button"> Voir Projet »</a></p>
        </div>
      @endforeach
      </div>-->
     

      </div> 
</div>
<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
          <!--<div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
          </div> -->
          <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
              <li><a href="/projects/{{ $project->id }}/edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier un projet</a></li>
              <li><a href="/projects"><i class="fa fa-user-o" aria-hidden="true"></i> Mes projets</a></li>
              <li><a href="/project/create"><i class="fa fa-plus-circle" aria-hidden="true"></i> Créer une nouveau projet</a></li>
               @if($project->user_id == Auth::user()->id)
              <li>
              <i class="fa fa-power-off" aria-hidden="true"></i>   
              <a   
              href="#"
                  onclick="
                  var result = confirm('Vous voulez vraiment supprimer ce projet?');
                      if( result ){
                              event.preventDefault();
                              document.getElementById('delete-form').submit();
                      }">
                  Supprimer
              </a>
              <form id="delete-form" action="{{ route('projects.destroy',[$project->id]) }}" 
                method="POST" style="display: none;">
                        <input type="hidden" name="_method" value="delete">
                        {{ csrf_field() }}
              </form>
            </div>
              </li>
          @endif
              <!-- <li><a href="#">Ajouter un nouveau membre</a></li> -->
            </ol>
            <hr/>
         <h4>Ajouter des membres</h4>
<div class="row">
              <div class="col-lg-12 col-md-12 col-xs-12  col-sm-12 ">
               <form id="add-user" action="{{ route('projects.adduser') }}" 
                method="POST">
                {{ csrf_field() }}
                <div class="input-group"> 
                  
                  <input class="form-control" placeholder="Email" type="text" name="email" >
                  <input class="form-control" type="hidden" value="{{$project->id}}" name="project_id">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="addMember" >Ajouter</button>
                  </span>
                </div><!-- /input-group -->
                </form>
              </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
            <br/>
             <h4>membres de l'équipe</h4>
            <ol class="list-unstyled">
              @foreach($project->users as $user)
              <li><a href="#">{{$user->email}}</a></li>
              @endforeach
              
            </ol>
          <!--<div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
              <li><a href="#">March 2014</a></li>
            </ol>
          </div> -->

        </div>


    @endsection